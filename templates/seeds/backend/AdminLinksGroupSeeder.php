<?php

use Illuminate\Database\Seeder;

class AdminLinksGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $links = [
            [1,	'Admininstración', 1],
            [2,	'Páginas', 3],
            [3,	'Content Manager', 2],
            [4,	'Solicitudes de contacto', 4],
        ];

        foreach ($links as $link) {

            \DB::table('admin_links_group')->insert(array(
                'id' => $link[0],
                'group_name' => $link[1],
                'order' => $link[2],
            ));
        }
    }
}