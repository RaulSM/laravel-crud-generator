<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin Language
    |--------------------------------------------------------------------------
    |
    | Textos para las páginas de administración
    |
    */
    'company' => 'RSMONDEJAR',
    'administration-page' => 'Administration Page',
    'close-session' => 'Close session',
    'intranet' => 'INTRANET',
    'welcome' => 'Welcome',
    'edit' => 'Edit',
    "save" => "Save",
    "add-new" => "Add New",
    "no-found-model-name" => "No :model-name found.",
    "action" => "Action",
    "delete-this-model" => "Are you sure wants to delete this :model-name?",
    "model-name-not-found" => ":model-name not found.",
    "model-name-save-successfully" => ":model-name saved successfully.",
    "model-name-updated-successfully" => ":model-name updated successfully.",
    "model-name-deleted-successfully" => ":model-name deleted successfully.",
    "model-name-retrieved-successfully" => ":model-name retrieved successfully.",




];
