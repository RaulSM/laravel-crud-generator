<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin Language
    |--------------------------------------------------------------------------
    |
    | Textos para las páginas de administración
    |
    */
    'company' => 'RSMONDEJAR',
    'administration-page' => 'Página de Administración',
    'close-session' => 'Cerrar sesión',
    'intranet' => 'INTRANET',
    'welcome' => 'Bienvenido',
    'edit' => 'Editar',
    "save" => "Guardar",
    "add-new" => "Añadir",
    "no-found-model-name" => "No :model-name encontrado.",
    "action" => "Acción",
    "delete-this-model" => "¿Estas seguro que deseas eliminar :model-name?",
    "model-name-not-found" => ":model-name no encontrado.",
    "model-name-save-successfully" => ":model-name guardado correctamente.",
    "model-name-updated-successfully" => ":model-name actualizado correctamente.",
    "model-name-deleted-successfully" => ":model-name elimado correctamente.",
    "model-name-retrieved-successfully" => ":model-name recuperado correctamente.",




];
