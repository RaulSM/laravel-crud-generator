<?php

namespace RSMCrud\Generator;

use Illuminate\Support\Str;

class FormFieldsGenerator
{
    public static function generateLabel($field)
    {
        $label = Str::title(str_replace('_', ' ', $field['fieldName']));

        $template = "{!! Form::label('\$FIELD_NAME\$', '\$FIELD_NAME_TITLE\$:') !!}";

        $template = str_replace('$FIELD_NAME_TITLE$', $label, $template);
        $template = str_replace('$FIELD_NAME$', $field['fieldName'], $template);

        return $template;
    }

    private static function replaceFieldVars($textField, $field)
    {
        $label = Str::title(str_replace('_', ' ', $field['fieldName']));

        $textField = str_replace('$FIELD_NAME$', $field['fieldName'], $textField);
        $textField = str_replace('$FIELD_NAME_TITLE$', $label, $textField);
        $textField = str_replace('$FIELD_INPUT$', $textField, $textField);

        return $textField;
    }

    public static function text($templateData, $field)
    {
        $textField = self::generateLabel($field);

        $textField .= "\n\t{!! Form::text('\$FIELD_NAME\$', null, ['class' => 'form-control']) !!}";

        $templateData = str_replace('$FIELD_INPUT$', $textField, $templateData);

        $templateData = self::replaceFieldVars($templateData, $field);

        return $templateData;
    }

    public static function textarea($templateData, $field)
    {
        $textareaField = self::generateLabel($field);

        // INI - RSM - 20160119 - Añadido clase wysiwyg
        //$textareaField .= "\n\t{!! Form::textarea('\$FIELD_NAME\$', null, ['class' => 'form-control']) !!}";
        $textareaField .= "\n\t{!! Form::textarea('\$FIELD_NAME\$', null, ['class' => 'form-control wysiwyg']) !!}";
        // FIN - RSM - 20160119 - Añadido clase wysiwyg

        $templateData = str_replace('$FIELD_INPUT$', $textareaField, $templateData);

        $templateData = self::replaceFieldVars($templateData, $field);

        return $templateData;
    }

    public static function password($templateData, $field)
    {
        $textField = self::generateLabel($field);

        $textField .= "\n\t{!! Form::password('\$FIELD_NAME\$', ['class' => 'form-control']) !!}";

        $templateData = str_replace('$FIELD_INPUT$', $textField, $templateData);

        $templateData = self::replaceFieldVars($templateData, $field);

        return $templateData;
    }

    public static function email($templateData, $field)
    {
        $textField = self::generateLabel($field);

        $textField .= "\n\t{!! Form::email('\$FIELD_NAME\$', null, ['class' => 'form-control']) !!}";
        $templateData = str_replace('$FIELD_INPUT$', $textField, $templateData);

        $templateData = self::replaceFieldVars($templateData, $field);

        return $templateData;
    }

    public static function file($templateData, $field)
    {
        $textField = self::generateLabel($field);

        $textField .= "\n\t{!! Form::file('\$FIELD_NAME\$') !!}";

        $templateData = str_replace('$FIELD_INPUT$', $textField, $templateData);

        $templateData = self::replaceFieldVars($templateData, $field);

        return $templateData;
    }

    public static function checkbox($templateData, $field)
    {
        $textField = "<div class=\"checkbox\">\n";
        $textField .= "\t\t<label>";

        $textField .= "{!! Form::checkbox('\$FIELD_NAME\$', 1, true) !!}";
        $textField .= '$FIELD_NAME_TITLE$';

        $textField .= '</label>';
        $textField .= "\n\t</div>";

        $templateData = str_replace('$FIELD_INPUT$', $textField, $templateData);

        $templateData = self::replaceFieldVars($templateData, $field);

        return $templateData;
    }

    public static function radio($templateData, $field)
    {
        $textField = self::generateLabel($field);

        if (count($field['typeOptions']) > 0) {
            $arr = explode(',', $field['typeOptions']);

            foreach ($arr as $item) {
                $label = Str::title(str_replace('_', ' ', $item));

                $textField .= "\n\t<div class=\"radio-inline\">";
                $textField .= "\n\t\t<label>";

                $textField .= "\n\t\t\t{!! Form::radio('\$FIELD_NAME\$', '".$item."', null) !!} $label";

                $textField .= "\n\t\t</label>";
                $textField .= "\n\t</div>";
            }
        }

        $templateData = str_replace('$FIELD_INPUT$', $textField, $templateData);

        $templateData = self::replaceFieldVars($templateData, $field);

        return $templateData;
    }

    public static function number($templateData, $field)
    {
        $textField = self::generateLabel($field);

        $textField .= "\n\t{!! Form::number('\$FIELD_NAME\$', null, ['class' => 'form-control']) !!}";
        $templateData = str_replace('$FIELD_INPUT$', $textField, $templateData);

        $templateData = self::replaceFieldVars($templateData, $field);

        return $templateData;
    }

    /**
     * @param $templateData
     * @param $field
     * @return mixed
     * @version 1.1 Se ha incluido la clase css datapicker y cambiado el tipo de campo a text para evitar problemas
     * con navegadores que no utilicen HTML5
     */
    public static function date($templateData, $field)
    {
        $textField = self::generateLabel($field);

        // INI - RSM - 20160119 - Añadido clase datapicker y cambiado a tipo TEXT
        //$textField .= "\n\t{!! Form::date('\$FIELD_NAME\$', null, ['class' => 'form-control']) !!}";
        $textField .= "\n\t{!! Form::text('\$FIELD_NAME\$', null, ['class' => 'form-control datepicker']) !!}";
        // FIN - RSM - 20160119 - Añadido clase datapicker y cambiado a tipo TEXT

        $templateData = str_replace('$FIELD_INPUT$', $textField, $templateData);

        $templateData = self::replaceFieldVars($templateData, $field);

        return $templateData;
    }

    public static function select($templateData, $field)
    {
        $textField = self::generateLabel($field);

        $textField .= "\n\t{!! Form::select('\$FIELD_NAME\$', \$INPUT_ARR\$, null, ['class' => 'form-control']) !!}";
        $textField = str_replace('$FIELD_NAME$', $field['fieldName'], $textField);

        if (count($field['typeOptions']) > 0) {
            $arr = explode(',', $field['typeOptions']);
            $inputArr = '[';
            foreach ($arr as $item) {
                $inputArr .= " '$item' => '$item',";
            }

            $inputArr = substr($inputArr, 0, strlen($inputArr) - 1);

            $inputArr .= ' ]';

            $textField = str_replace('$INPUT_ARR$', $inputArr, $textField);
        } else {
            $textField = str_replace('$INPUT_ARR$', '[]', $textField);
        }

        $templateData = str_replace('$FIELD_INPUT$', $textField, $templateData);

        $templateData = self::replaceFieldVars($templateData, $field);

        return $templateData;
    }

    // INI - RSM - 20160119 - Añadido tipo de campo color
    /**
     * Función para por elegir color
     * @autor RSMONDEJAR
     * @param $templateData
     * @param $field
     * @return mixed
     */
    public static function color($templateData, $field)
    {
        $textField = self::generateLabel($field);

        $textField .= "\n\t{!! Form::text('\$FIELD_NAME\$', null, ['class' => 'form-control jscolor {hash:true}']) !!}";

        $templateData = str_replace('$FIELD_INPUT$', $textField, $templateData);

        $templateData = self::replaceFieldVars($templateData, $field);

        return $templateData;
    }
    // FIN - RSM - 20160119 - Añadido tipo de campo color


    // INI - RSM - 20160119 - Añadido tipo fecha y hora
    /**
     * Función para poder elegir fecha y hora utilizando las librerias de jquery-ui
     * @param $templateData
     * @param $field
     * @return mixed
     */
    public static function dateTime($templateData, $field)
    {
        $textField = self::generateLabel($field);

        $textField .= "\n\t{!! Form::text('\$FIELD_NAME\$', null, ['class' => 'form-control datetimepicker']) !!}";
        $templateData = str_replace('$FIELD_INPUT$', $textField, $templateData);

        $templateData = self::replaceFieldVars($templateData, $field);

        return $templateData;
    }
    // FIN - RSM - 20160119 - Añadido tipo fecha y hora

    // INI - RSM - 20160216 - Añadido tipo file imagen

    // @TODO REVISAR FUNCION INSERTAR IMAGEN

    /**
     * Función para cargar campo file y miniatura de la imagen
     * @param $templateData
     * @param $field
     * @return mixed
     */
    public static function image($templateData, $field)
    {
        $textField = self::generateLabel($field);


        $textField .= "\n\t{!! Form::file('\$FIELD_NAME\$', ['class' => 'imagen-input-file', 'value' => (isset(\$MODEL_NAME_PLURAL_CAMEL->\$FIELD_NAME\$)) ? \$MODEL_NAME_PLURAL_CAMEL->\$FIELD_NAME\$ : '' ]) !!}";

        $textField .= "<div class='img-thumb-container'>\n";
        $textField .= "<a href='{{ (isset(\$MODEL_NAME_PLURAL_CAMEL->\$FIELD_NAME\$) && (strlen(\$MODEL_NAME_PLURAL_CAMEL->\$FIELD_NAME\$) > 3)) ? '/files/' . \$MODEL_NAME_PLURAL_CAMEL->\$FIELD_NAME\$ : '/admin/img/no-image.png' }}' target='_blank' title='Zoom' >\n";

        $textField .= "<img src='{{ (isset(\$MODEL_NAME_PLURAL_CAMEL->\$FIELD_NAME\$) && (strlen(\$MODEL_NAME_PLURAL_CAMEL->\$FIELD_NAME\$) > 3)) ? '/files/' . \$MODEL_NAME_PLURAL_CAMEL->\$FIELD_NAME\$ : '/admin/img/no-image.png' }}' class='img-thumb' alt='{{ \$MODEL_NAME_PLURAL_CAMEL->\$FIELD_NAME\$ or '' }}' />\n";
        $textField .= "</a>\n";

        $textField .= "<button class='btn btn-default input-file-detete'>\n";
        $textField .= "<i class='fa fa-remove'> Elimnar</i>\n";
        $textField .= "</button>\n";
        $textField .= "</div>";


        $textField .= "\n\t{!! Form::file('\$FIELD_NAME\$', ".
            '["class" => "imagen-input-file", '.
            ' "value" => (isset($FIELD_INPUT$)) ? $FIELD_INPUT$ : "" ]) !!}';


        $textField .= '<div class="img-thumb-container">';
        $textField .= '<a href="{{ (isset($FIELD_INPUT$)) ? \'/files/\'.$FIELD_INPUT$ : \'/admin/img/no-image.png\' }}" target="_blank" title="Zoom">';
        $textField .= '<img src="{{ (isset($FIELD_INPUT$)) ? \'/files/\'.$FIELD_INPUT$ : \'/admin/img/no-image.png\' }}" class="img-thumb" alt="" />';
        $textField .= '</a>';
        $textField .= '<button class="btn btn-default input-file-detete">';
        $textField .= '<i class="fa fa-remove"> Elimnar</i>';
        $textField .= '</button>';
        $textField .= '</div>';


        $templateData = str_replace('$FIELD_INPUT$', $textField, $templateData);

        $templateData = self::replaceFieldVars($templateData, $field);

        return $templateData;
    }
    // FIN - RSM - 20160216 - Añadido tipo file imagen
}
