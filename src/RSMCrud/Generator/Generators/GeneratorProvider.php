<?php

namespace RSMCrud\Generator\Generators;

interface GeneratorProvider
{
    public function generate();
}
