<?php

namespace RSMCrud\Generator\Generators\Frontend;

use Config;
use RSMCrud\Generator\CommandData;
use RSMCrud\Generator\Generators\GeneratorProvider;
use RSMCrud\Generator\Utils\GeneratorUtils;


class FrontViewGenerator implements GeneratorProvider
{
    /** @var  CommandData */
    private $commandData;

    /** @var string */
    private $path;

    /** @var string */
    private $viewsPath;


    public function __construct($commandData)
    {
        $this->commandData = $commandData;

        $this->path = Config::get('generator.path_views', base_path('resources/views')).'/'.$this->commandData->modelNameSnakeCase.'/';

        $this->viewsPath = 'frontend/views';


    }

    public function generate()
    {
        if (!file_exists($this->path)) {
            mkdir($this->path, 0755, true);
            $this->commandData->commandObj->comment("\nViews created: ");
        }

        $this->generateIndex();
    }


    private function generateIndex()
    {
        $templateData = $this->commandData->templatesHelper->getTemplate('index.blade', $this->viewsPath);

        $templateData = GeneratorUtils::fillTemplate($this->commandData->dynamicVars, $templateData);

        if ($this->commandData->paginate) {
            $paginateTemplate = $this->commandData->templatesHelper->getTemplate('paginate.blade', 'frontend/views');

            $paginateTemplate = GeneratorUtils::fillTemplate($this->commandData->dynamicVars, $paginateTemplate);

            $templateData = str_replace('$PAGINATE$', $paginateTemplate, $templateData);
        } else {
            $templateData = str_replace('$PAGINATE$', '', $templateData);
        }

        $fileName = 'index.blade.php';

        $path = $this->path.$fileName;

        if (file_exists($path)) {

            $answer = $this->commandData->commandObj->ask('Do you want to overwrite '.$fileName.'? (y|N) :', false);

            if (strtolower($answer) != 'y' and strtolower($answer) != 'yes') {
                return;
            }
        }

        $this->commandData->fileHelper->writeFile($path, $templateData);
        $this->commandData->commandObj->info($fileName . ' created');
    }


}
