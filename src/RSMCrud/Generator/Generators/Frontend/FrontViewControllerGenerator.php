<?php

namespace RSMCrud\Generator\Generators\Frontend;

use Config;
use Illuminate\Console\Command;
use RSMCrud\Generator\CommandData;
use RSMCrud\Generator\Generators\GeneratorProvider;
use RSMCrud\Generator\Utils\GeneratorUtils;

class FrontViewControllerGenerator extends Command implements GeneratorProvider
{
    /** @var  CommandData */
    private $commandData;

    /** @var string */
    private $path;

    public function __construct($commandData)
    {
        $this->commandData = $commandData;
        $this->path = Config::get('generator.path_controller', app_path('Http/Controllers'));
    }

    public function generate()
    {
        $templateData = $this->commandData->templatesHelper->getTemplate('FrontController', 'frontend');

        $templateData = GeneratorUtils::fillTemplate($this->commandData->dynamicVars, $templateData);

        if ($this->commandData->paginate) {
            $templateData = str_replace('$RENDER_TYPE$', 'paginate('.$this->commandData->paginate.')', $templateData);
        } else {
            $templateData = str_replace('$RENDER_TYPE$', 'all()', $templateData);
        }

        $fileName = $this->commandData->modelName.'Controller.php';

        $path = $this->path.$fileName;

        if (file_exists($path)) {

            $answer = $this->commandData->commandObj->ask('Do you want to overwrite '.$fileName.'? (y|N) :', false);

            if (strtolower($answer) != 'y' and strtolower($answer) != 'yes') {
                return;
            }
        }

        $this->commandData->fileHelper->writeFile($path, $templateData);
        $this->commandData->commandObj->comment("\nFrontController created: ");
        $this->commandData->commandObj->info($fileName);

    }
}
