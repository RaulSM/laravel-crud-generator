<?php

namespace RSMCrud\Generator;

use Illuminate\Support\ServiceProvider;
use RSMCrud\Generator\Commands\APIGeneratorCommand;
use RSMCrud\Generator\Commands\FrontendGeneratorCommand;
use RSMCrud\Generator\Commands\PublisherCommand;
use RSMCrud\Generator\Commands\ScaffoldAPIGeneratorCommand;
use RSMCrud\Generator\Commands\ScaffoldGeneratorCommand;

class GeneratorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $configPath = __DIR__.'/../../../config/generator.php';

        $this->publishes([
            $configPath => config_path('generator.php'),
        ]);


    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('rsmcrud.generator.publish', function ($app) {
            return new PublisherCommand();
        });

        $this->app->singleton('rsmcrud.generator.api', function ($app) {
            return new APIGeneratorCommand();
        });

        $this->app->singleton('rsmcrud.generator.scaffold', function ($app) {
            return new ScaffoldGeneratorCommand();
        });

        $this->app->singleton('rsmcrud.generator.scaffold_api', function ($app) {
            return new ScaffoldAPIGeneratorCommand();
        });

        // INI - RSM - 20160123 -Incluir comando para generar FrontEnd
        $this->app->singleton('rsmcrud.generator.frontend', function ($app) {
            return new FrontendGeneratorCommand();
        });
        // FIN - RSM - 20160123 -Incluir comando para generar FrontEnd

        $this->commands([
            'rsmcrud.generator.publish',
            'rsmcrud.generator.api',
            'rsmcrud.generator.scaffold',
            'rsmcrud.generator.scaffold_api',

            // INI - RSM - 20160123 -Incluir comando para generar FrontEnd
            'rsmcrud.generator.frontend',
            // FIN - RSM - 20160123 -Incluir comando para generar FrontEnd
        ]);
    }
}
