<?php

namespace RSMCrud\Generator\Commands;

use RSMCrud\Generator\CommandData;
use RSMCrud\Generator\Generators\Common\MigrationGenerator;
use RSMCrud\Generator\Generators\Common\ModelGenerator;
use RSMCrud\Generator\Generators\Common\RepositoryGenerator;
use RSMCrud\Generator\Generators\Common\RequestGenerator;
use RSMCrud\Generator\Generators\Common\RoutesGenerator;
use RSMCrud\Generator\Generators\Scaffold\ViewControllerGenerator;
use RSMCrud\Generator\Generators\Scaffold\ViewGenerator;

class ScaffoldGeneratorCommand extends BaseCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'rsmcrud.generator:scaffold';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a full CRUD for given model with initial views';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();

        $this->commandData = new CommandData($this, CommandData::$COMMAND_TYPE_SCAFFOLD);
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        parent::handle();

        if (!$this->commandData->skipMigration and !$this->commandData->fromTable) {
            $migrationGenerator = new MigrationGenerator($this->commandData);
            $migrationGenerator->generate();
        }

        $modelGenerator = new ModelGenerator($this->commandData);
        $modelGenerator->generate();

        $requestGenerator = new RequestGenerator($this->commandData);
        $requestGenerator->generate();

        $repositoryGenerator = new RepositoryGenerator($this->commandData);
        $repositoryGenerator->generate();

        $repoControllerGenerator = new ViewControllerGenerator($this->commandData);
        $repoControllerGenerator->generate();

        $viewsGenerator = new ViewGenerator($this->commandData);
        $viewsGenerator->generate();

        $routeGenerator = new RoutesGenerator($this->commandData);
        $routeGenerator->generate();


        // INI - RSM - 20160123 - Crear condicional para que no pregunta por la migración si se ejecuta el comando "--skipMigration"
        /*
        if ($this->confirm("\nDo you want to migrate database? [y|N]", false)) {
            $this->call('migrate');
        }
        */
        if (!$this->commandData->skipMigrationDatabase){
            if ($this->confirm("\nDo you want to migrate database? [y|N]", false)) {
                $this->call('migrate');
            }
        }
        // FIN - RSM - 20160123 - Crear condicional para que no pregunta por la migración si se ejecuta el comando "--skipMigration"


    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array_merge(parent::getArguments());
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    public function getOptions()
    {
        return array_merge(parent::getOptions(), []);
    }
}
