<?php

namespace RSMCrud\Generator\Commands;

use Config;
use File;
use Illuminate\Console\Command;
use RSMCrud\Generator\CommandData;
use RSMCrud\Generator\File\FileHelper;
use RSMCrud\Generator\TemplatesHelper;
use RSMCrud\Generator\Utils\GeneratorUtils;
use Symfony\Component\Console\Input\InputOption;

class PublisherCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'rsmcrud.generator:publish';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Publishes a various things of generator.';

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->option('all')) {
            // INI - RSM - 20160121 - Optimizacion del código
            $this->publishTemplates();
            $this->publishAppBaseController();

            $this->publishAdmin();
            $this->publishFrontend();

            // INI - RSM - 20160131 - Preguntar si se desea realizar la migración
            $this->doMigrationAndSeeder();
            // FINI - RSM - 20160131 - Preguntar si se desea realizar la migración

            // FIN - RSM - 20160121 - Optimizacion del código

        } elseif ($this->option('templates')) {
            $this->publishTemplates();
        } elseif ($this->option('baseController')) {
            $this->publishAppBaseController();
        } else {

            // INI - RSM - 20160121 - Optimizacion del código
            $this->publishAdmin();
            $this->publishFrontend();

            // INI - RSM - 20160131 - Preguntar si se desea realizar la migración
            $this->doMigrationAndSeeder();
            // FINI - RSM - 20160131 - Preguntar si se desea realizar la migración

            // FIN - RSM - 20160121 - Optimizacion del código
        }
    }

    // INI - RSM - 20160121 - Optimizacion del código
    private function publishAdmin() {
        $this->publishCommonViews();
        $this->publishAPIRoutes();
        $this->initAPIRoutes();

        // INI - RSM - 20160119 - Separar las rutas que se generan automaticamente
        $this->publishAdminRoutes();
        $this->initAdminRoutes();
        // FIN - RSM - 20160119 - Separar las rutas que se generan automaticamente

        // INI - RSM - 20160119 - Crear la carpeta "admin" dentro de la carpeta de controladores
        $this->publishAdminControllerFolder();
        // FIN - RSM - 20160119 - Crear la carpeta "admin" dentro de la carpeta de controladores

        // INI - RSM - 20160121 - Funcion para publicar la carpeta "admin" para las vistas
        $this->publishAdminViewFolder();
        // FIN - RSM - 20160121 - Funcion para publicar la carpeta "admin" para las vistas

        // INI - RSM - 20160120 - Publicar layouts del panel de administracion
        $this->publishAdminLayoutViews();
        // FIN - RSM - 20160120 - Publicar layouts del panel de administracion

        // INI - RSM - 20160120 - Publicar los ficheros necesario para el funcionamiento del backoffice.
        $this->publishAdminPublicFiles();
        // FIN - RSM - 20160120 - Publicar los ficheros necesario para el funcionamiento del backoffice.


        // INI - RSM - 20160120 - Publicar migraciones y seeds
        $this->publishMigrations();
        $this->publishSeeds();
        // FIN - RSM - 20160120 - Publicar migraciones y seeds

        // INI - RSM - 20160121 - Publicar los ficheros de idiomas para el funcionamiento del backoffice.
        $this->publishAdminLangs();
        // FIN - RSM - 20160121 - Publicar los ficheros de idiomas para el funcionamiento del backoffice.

        exec('composer dump-autoload');


    }
    // FIN - RSM - 20160121 - Optimizacion del código

    // INI - RSM - 20160131 - Preguntar si se desea realizar la migración
    private function doMigrationAndSeeder() {
        exec('composer dump-autoload');
        if ($this->confirm("\nDo you want to migrate database? [y|N]", false)) {
            $this->call('migrate');
            $this->call('db:seed');
        }
    }

    // FIN - RSM - 20160131 - Preguntar si se desea realizar la migración




    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    public function getOptions()
    {
        return [
            ['templates', null, InputOption::VALUE_NONE, 'Publish templates'],
            ['baseController', null, InputOption::VALUE_NONE, 'Publish base controller'],
            ['all', null, InputOption::VALUE_NONE, 'Publish all options'],
        ];
    }

    /**
     * Publishes templates.
     */
    public function publishTemplates()
    {
        $templatesPath = __DIR__.'/../../../../templates';

        $templatesCopyPath = base_path('resources/api-generator-templates');

        $this->publishDirectory($templatesPath, $templatesCopyPath, 'templates');
    }

    /**
     * Publishes common views.
     */
    public function publishCommonViews()
    {
        $viewsPath = __DIR__.'/../../../../views/common';

        $viewsCopyPath = base_path('resources/views/common');

        $this->publishDirectory($viewsPath, $viewsCopyPath, 'common views');
    }

    /**
     * Publishes base controller.
     */
    private function publishAppBaseController()
    {
        $templateHelper = new TemplatesHelper();
        $templateData = $templateHelper->getTemplate('AppBaseController', 'controller');

        $templateData = GeneratorUtils::fillTemplate(CommandData::getConfigDynamicVariables(), $templateData);

        $fileName = 'AppBaseController.php';

        // INI - RSM - 20160119 - Modificar la ruta donde se guardan los controladores del area de administracion
        //$filePath = Config::get('generator.path_controller', app_path('Http/Controllers'));
        $filePath = Config::get('generator.path_admin_controller', app_path('Http/Controllers/Admin'));
        // FIN - RSM - 20160119 - Modificar la ruta donde se guardan los controladores del area de administracion

        $fileHelper = new FileHelper();
        $fileHelper->writeFile($filePath.$fileName, $templateData);
        $this->comment("\nAppBaseController generated");
        //$this->info($fileName);
    }

    /**
     * Publishes api_routes.php.
     */
    public function publishAPIRoutes()
    {
        $routesPath = __DIR__.'/../../../../templates/routes/api_routes.stub';

        $apiRoutesPath = Config::get('generator.path_api_routes', app_path('Http/api_routes.php'));

        $this->publishFile($routesPath, $apiRoutesPath, 'api_routes.php');
    }




    public function publishFile($sourceFile, $destinationFile, $fileName)
    {
        if (file_exists($destinationFile)) {
            $answer = $this->ask('Do you want to overwrite '.$fileName.'? (y|N) :', false);

            if (strtolower($answer) != 'y' and strtolower($answer) != 'yes') {
                return;
            }
        }

        copy($sourceFile, $destinationFile);

        $this->comment($fileName.' generated');
        $this->info($destinationFile);
    }

    public function publishDirectory($sourceDir, $destinationDir, $dirName)
    {
        if (file_exists($destinationDir)) {
            $answer = $this->ask('Do you want to overwrite '.$dirName.'? (y|N) :', false);

            if (strtolower($answer) != 'y' and strtolower($answer) != 'yes') {
                return;
            }
        } else {
            File::makeDirectory($destinationDir);
        }

        File::copyDirectory($sourceDir, $destinationDir);

        $this->comment($dirName.' published');
        $this->info($destinationDir);
    }

    /**
     * Initialize routes group based on route integration.
     */
    private function initAPIRoutes()
    {
        $path = Config::get('generator.path_routes', app_path('Http/routes.php'));

        $fileHelper = new FileHelper();
        $routeContents = $fileHelper->getFileContents($path);

        $useDingo = Config::get('generator.use_dingo_api', false);

        if ($useDingo) {
            $template = 'dingo_api_routes_group';
        } else {
            $template = 'api_routes_group';
        }

        $templateHelper = new TemplatesHelper();
        $templateData = $templateHelper->getTemplate($template, 'routes');

        $templateData = $this->fillTemplate($templateData);

        $fileHelper->writeFile($path, $routeContents."\n\n".$templateData);
        $this->comment("\nAPI group added to routes.php");
    }


    /**
     * Replaces dynamic variables of template.
     *
     * @param string $templateData
     *
     * @return string
     */
    private function fillTemplate($templateData)
    {
        $apiVersion = Config::get('generator.api_version');
        $apiPrefix = Config::get('generator.api_prefix');
        $apiNamespace = Config::get('generator.namespace_api_controller');

        $templateData = str_replace('$API_VERSION$', $apiVersion, $templateData);
        $templateData = str_replace('$NAMESPACE_API_CONTROLLER$', $apiNamespace, $templateData);
        $templateData = str_replace('$API_PREFIX$', $apiPrefix, $templateData);

        // INI - RSM - 20160119 - Sustituir las nueva variables que han sido generadas
        $adminPrefix = Config::get('generator.admin_prefix');
        $templateData = str_replace('$ADMIN_PREFIX$', $adminPrefix, $templateData);
        // FIN - RSM - 20160119 - Sustituir las nueva variables que han sido generadas

        return $templateData;
    }

    // INI - RSM - 20160119 - Función para guardar las rutas del panel de administración
    /**
     * Publishes admin_routes.php.
     */
    public function publishAdminRoutes()
    {
        $routesPath = __DIR__.'/../../../../templates/routes/admin_routes.stub';

        $apiRoutesPath = Config::get('generator.path_admin_routes', app_path('Http/admin_routes.php'));

        $this->publishFile($routesPath, $apiRoutesPath, 'admin_routes.php');

    }
    // FIN - RSM - 20160119 - Función para guardar las rutas del panel de administración

    // INI - RSM - 20160119 - Funcion para publicar la carpeta "Admin" para los controladores
    private function publishAdminControllerFolder()
    {
        $adminControllerPath = Config::get('generator.path_admin_controller', app_path('Http/Controllers/Admin/'));

        $adminFolder = ucfirst(Config::get('generator.admin_prefix', 'admin'));

        $this->publishDirectory($adminFolder, $adminControllerPath, 'Admin Controller Folder');
    }
    // FIN - RSM - 20160119 - Funcion para publicar la carpeta "Admin" para los controladores


    /**
     * RSM
     * Inicializar las rutas que se generan automaticamente
     */
    private function initAdminRoutes()
    {
        $path = Config::get('generator.path_routes', app_path('Http/routes.php'));

        $fileHelper = new FileHelper();
        $routeContents = $fileHelper->getFileContents($path);

        $template = 'admin_routes_group';

        $templateHelper = new TemplatesHelper();
        $templateData = $templateHelper->getTemplate($template, 'routes');

        $templateData = $this->fillTemplate($templateData);

        $fileHelper->writeFile($path, $routeContents."\n\n".$templateData);
        $this->comment("\nAdmin group added to routes.php");
    }


    // INI - RSM - 20160120 - Crear la carpeta "layout" dentro de las vistas y copiar la plantilla
    /**
     * Publishes admin layouts views.
     */
    private function publishAdminLayoutViews()
    {
        $templateHelper = new TemplatesHelper();
        $templateData = $templateHelper->getTemplate('app.blade', 'scaffold/views');

        $templateData = GeneratorUtils::fillTemplate(CommandData::getConfigDynamicVariables(), $templateData);

        $fileName = 'app.blade.php';

        $filePath = Config::get('generator.path_admin_views_layout', base_path('resources/views/admin/layouts'))."/";

        $this->publishDirectory($fileName, $filePath, 'Admin Layout base Folder');


        $destinationPath = $filePath.$fileName;
        if (file_exists($destinationPath)) {
            $answer = $this->ask('Do you want to overwrite '.$fileName.'? (y|N) :', false);

            if (strtolower($answer) != 'y' and strtolower($answer) != 'yes') {
                return;
            }
        } else {
            $fileHelper = new FileHelper();

            $fileHelper->writeFile($destinationPath, $templateData);
            $this->comment("\napp.blade generated");
        }


        // INI - RSM - 20160120 - Publicar los partials de las vistas de administracion
        $this->publishAdminPartialsViews();
        // FIN - RSM - 20160120 - Publicar los partials de las vistas de administracion

    }
    // FIN - RSM - 20160120 - Crear la carpeta "layout" dentro de las vistas y copiar la plantilla

    // INI - RSM - 20160120 - Publicar los partials de las vistas de administracion
    /**
     * Publishes admin partials views
     */
    private function publishAdminPartialsViews()
    {
        $templateHelper = new TemplatesHelper();
        $fileHelper = new FileHelper();

        $partials = [
            "aside-left.blade",
            "footer.blade",
            "login.blade",
            "metas.blade",
            "navbar-top.blade",
            "scripts.blade",
            "styles.blade"
        ];

        $filePath = Config::get('generator.path_admin_views_partials', base_path('resources/views/admin/partials'))."/";

        $this->publishDirectory('partials', $filePath, 'Admin Layout base Folder');

        foreach($partials as $partial) {
            $templateData = $templateHelper->getTemplate($partial, 'scaffold/views/partials');
            $templateData = GeneratorUtils::fillTemplate(CommandData::getConfigDynamicVariables(), $templateData);
            $fileName = $partial.'.php';

            $fileHelper->writeFile($filePath.$fileName, $templateData);
            $this->comment("$partial generated");

        }

    }
    // FIN - RSM - 20160120 - Publicar los partials de las vistas de administracion



    // INI - RSM - 20160120 - Publicar los ficheros necesario para el funcionamiento del backoffice.
    private function publishAdminPublicFiles()
    {
        $backofficeFilesPath = __DIR__.'/../../../../backofficeFiles';

        $backofficeFilesCopyPath = Config::get('generator.path_admin_public', base_path('public/admin/'));

        $this->publishDirectory($backofficeFilesPath, $backofficeFilesCopyPath, 'BackOffice Files');
    }
    // FIN - RSM - 20160120 - Publicar los ficheros necesario para el funcionamiento del backoffice.


    // INI - RSM - 20160120 - Publicar migraciones y seeds
    private function publishMigrations()
    {
        $templateHelper = new TemplatesHelper();
        $fileHelper = new FileHelper();

        $migrationCopyPath = Config::get('generator.path_migration', base_path('database/migrations/'));

        $migrations = [
            "0000_00_00_001000_create_langs_table",
            "0000_00_00_002000_create_admin_links_table",
        ];

        foreach($migrations as $migration) {
            $templateData = $templateHelper->getTemplate($migration, 'migration/backend');
            $templateData = GeneratorUtils::fillTemplate(CommandData::getConfigDynamicVariables(), $templateData);
            $fileName = $migration.'.php';

            $fileHelper->writeFile($migrationCopyPath.$fileName, $templateData);
            $this->comment("$fileName generated");
        }
    }

    private function publishSeeds()
    {
        $templateHelper = new TemplatesHelper();
        $fileHelper = new FileHelper();

        $seedCopyPath = Config::get('generator.path_seeds', base_path('database/seeds/'));

        $seeds = [
            "LangsSeeder",
            "AdminLinksSeeder",
            "DatabaseSeeder",
        ];

        foreach($seeds as $seed) {
            $templateData = $templateHelper->getTemplate($seed, 'seeds/backend');
            $templateData = GeneratorUtils::fillTemplate(CommandData::getConfigDynamicVariables(), $templateData);
            $fileName = $seed.'.php';

            $fileHelper->writeFile($seedCopyPath.$fileName, $templateData);
            $this->comment("$fileName generated");
        }
    }
    // FIN - RSM - 20160120 - Publicar migraciones y seeds

    // INI - RSM - 20160121 - Publicar los ficheros de idiomas para el funcionamiento del backoffice.
    /**
     * Funcion para publicar los idiomas del backoffice
     */
    private function publishAdminLangs()
    {
        $langsFilesPath = __DIR__.'/../../../../lang';

        $langsFilesCopyPath = Config::get('generator.path_langs', base_path('resources/lang/'));

        $this->publishDirectory($langsFilesPath, $langsFilesCopyPath, 'Langs Files');
    }
    // FIN - RSM - 20160121 - Publicar los ficheros de idiomas para el funcionamiento del backoffice.


    // INI - RSM - 20160121 - Funcion para publicar la carpeta "admin" para las vistas
    /**
     * Funcion para publicar la carpeta "admin" dentro de las vistas
     */
    private function publishAdminViewFolder()
    {
        $adminViewPath = Config::get('generator.path_admin_views', base_path('resources/views/admin'));

        $adminViewFolder = Config::get('generator.admin_prefix', 'admin');

        $this->publishDirectory($adminViewFolder, $adminViewPath, 'Admin View Folder');
    }
    // FIN - RSM - 20160121 - Funcion para publicar la carpeta "admin" para las vistas


    // INI - RSM - 20160124 - Funcion para publicar el Frontend: controller, rutas, vistas
    //<editor-fold defaultstate="collapsed" desc="private function publishFrontend()">
    /**
     * Funcion para publicar el Frontend: controller, rutas, vistas
     */
    private function publishFrontend(){
        // Preguntar si se desea publicar el FrontEnd
        $answer = $this->ask('Do you want to install FrontEnd? (y|N) :', false);

        if (strtolower($answer) === 'y' || strtolower($answer) === 'yes') {

            // INI - RSM - 20160121 - Publicar el controlador del FrontEnd
            $this->publishFrontAppBaseController();
            // FIN - RSM - 20160121 - Publicar el controlador del FrontEnd

            // INI - RSM - 20160121 - Publicar las vistas base del FrontEnd
            $this->generatePartialsFolder();
            $this->publishPartialsViews();
            $this->generateBaseLayoutFolder();
            $this->generateBaseLayoutFile();
            // FIN - RSM - 20160121 - Publicar las vistas base del FrontEnd

            // INI - RSM - 20160126 - Publicar migraciones, seeds, rutas y vistas para el FrontEnd
            $this->publishFrontEndMigrations();
            // FIN - RSM - 20160126 - Publicar migraciones, seeds, rutas y vistas para el FrontEnd


            // INI - RSM - 20160131 - Preguntar si se desea crear ejemplos del frontend
            $answer = $this->ask('Do you want to install demo FrontEnd? (y|N) :', false);

            if (strtolower($answer) === 'y' || strtolower($answer) === 'yes') {
                $this->publishFrontEndSeeds();
                $this->initFrontEndDemoRoutes();
                $this->publishFrontEndDemoViewFolder();
                $this->publishFrontEndDemoFilesViews();
                $this->publishFrontDemoController();

            }
            // FIN - RSM - 20160131 - Preguntar si se desea crear ejemplos del frontend


        }
    }
    // FIN - RSM - 20160124 - Funcion para publicar el Frontend: controller, rutas, vistas


    // INI - RSM - 20160121 - Publicar el controlador del FrontEnd
    private function publishFrontAppBaseController()
    {
        $templateHelper = new TemplatesHelper();
        $templateData = $templateHelper->getTemplate('FrontAppBaseController', 'controller');

        $templateData = GeneratorUtils::fillTemplate(CommandData::getConfigDynamicVariables(), $templateData);

        $fileName = 'FrontAppBaseController.php';

        $filePath = Config::get('generator.path_controller', app_path('Http/Controllers'));

        $fileHelper = new FileHelper();
        $fileHelper->writeFile($filePath.$fileName, $templateData);
        $this->comment("\n$fileName generated");

    }
    // FIN - RSM - 20160121 - Publicar el controlador del FrontEnd


    /**
     * Funcion para publicar la carpeta "partials" dentro de las vistas
     */
    private function generatePartialsFolder() {

        $layoutPartialsViewPath = Config::get('generator.path_views_partials_folder', base_path('resources/views/partials')).'/';
        if (!file_exists($layoutPartialsViewPath)) {
            mkdir($layoutPartialsViewPath, 0755, true);
            $this->comment("\nPartials Folder created: ");
        }
    }


    // INI - RSM - 20160124 - Publicar los partials de las vistas frontend
    /**
     * Publishes admin partials views
     */
    private function publishPartialsViews()
    {
        $templateHelper = new TemplatesHelper();
        $fileHelper = new FileHelper();

        $partials = [
            "favicon.blade",
            "footer.blade",
            "menu.blade",
            "metas.blade",
            "scripts.blade",
            "styles.blade"
        ];

        $filePath = Config::get('generator.path_views_partials_folder', base_path('resources/views/partials'))."/";

        foreach($partials as $partial) {
            $templateData = $templateHelper->getTemplate($partial, 'frontend/views/partials');
            $templateData = GeneratorUtils::fillTemplate(CommandData::getConfigDynamicVariables(), $templateData);
            $fileName = $partial.'.php';

            $fileHelper->writeFile($filePath.$fileName, $templateData);
            $this->comment("$partial generated");

        }

    }
    // FIN - RSM - 20160124 - Publicar los partials de las vistas frontend


    /**
     * Funcion para publicar la carpeta "layouts" dentro de las vistas
     */
    private function generateBaseLayoutFolder() {
        $layoutBaseFolderViewPath = Config::get('generator.path_views_layout_folder', base_path('resources/views/layouts')).'/';
        if (!file_exists($layoutBaseFolderViewPath)) {
            mkdir($layoutBaseFolderViewPath, 0755, true);
            $this->comment("\nLayout Base Folder created: ");
        }
    }


    /**
     * Funcion para publicar el fichero base layout
     */
    private function generateBaseLayoutFile()
    {
        $templateHelper = new TemplatesHelper();
        $fileHelper = new FileHelper();

        $templateData = $templateHelper->getTemplate('base.blade', 'frontend/views');
        $templateData = GeneratorUtils::fillTemplate(CommandData::getConfigDynamicVariables(), $templateData);

        $fileName = 'base.blade.php';

        $filePath = Config::get('generator.path_views_layout_folder', base_path('resources/views/layouts'))."/";

        $path = $filePath.$fileName;

        if (file_exists($path)) {

            $answer = $this->ask('Do you want to overwrite '.$fileName.'? (y|N) :', false);

            if (strtolower($answer) != 'y' and strtolower($answer) != 'yes') {
                return;
            }
        }

        $fileHelper->writeFile($filePath.$fileName, $templateData);
        $this->comment("$fileName generated");
    }

    // INI - RSM - 20160126 - Publicar las migraciones para el FrontEnd
    private function publishFrontEndMigrations()
    {
        $templateHelper = new TemplatesHelper();
        $fileHelper = new FileHelper();

        $migrationCopyPath = Config::get('generator.path_migration', base_path('database/migrations/'));

        $migrations = [
            "0000_00_00_003000_create_links_table",
        ];

        foreach($migrations as $migration) {
            $templateData = $templateHelper->getTemplate($migration, 'migration/frontend');
            $templateData = GeneratorUtils::fillTemplate(CommandData::getConfigDynamicVariables(), $templateData);
            $fileName = $migration.'.php';

            $fileHelper->writeFile($migrationCopyPath.$fileName, $templateData);
            $this->comment("$fileName generated");
        }
    }
    // FIN - RSM - 20160126 - Publicar las migraciones para el FrontEnd

    // INI - RSM - 20160126 - Publicar los seeds para el FrontEnd
    private function publishFrontEndSeeds()
    {
        $templateHelper = new TemplatesHelper();
        $fileHelper = new FileHelper();

        $seedCopyPath = Config::get('generator.path_seeds', base_path('database/seeds/'));

        $seeds = [
            "LinksSeeder",
            "DatabaseSeeder",
        ];

        foreach($seeds as $seed) {
            $templateData = $templateHelper->getTemplate($seed, 'seeds/frontend');
            $templateData = GeneratorUtils::fillTemplate(CommandData::getConfigDynamicVariables(), $templateData);
            $fileName = $seed.'.php';

            $fileHelper->writeFile($seedCopyPath.$fileName, $templateData);
            $this->comment("$fileName generated");
        }
    }
    // FIN - RSM - 20160126 - Publicar los seeds para el FrontEnd


    // INI - RSM - 20160126 - Publicar rutas demo para el FrontEnd
    /**
     * Publicar rutas demo para el FrontEnd
     */
    private function initFrontEndDemoRoutes()
    {
        $path = Config::get('generator.path_routes', app_path('Http/routes.php'));

        $fileHelper = new FileHelper();
        $routeContents = $fileHelper->getFileContents($path);

        $template = 'frontend_routes_demo';

        $templateHelper = new TemplatesHelper();
        $templateData = $templateHelper->getTemplate($template, 'routes');

        $templateData = $this->fillTemplate($templateData);

        $fileHelper->writeFile($path, $routeContents."\n\n".$templateData);
        $this->comment("\nFrontEnd Demo routes added to routes.php");
    }
    // FIN - RSM - 20160126 - Publicar rutas demo para el FrontEnd


    /**
     * Funcion para publicar la carpeta "demo" dentro de las vistas
     */
    private function publishFrontEndDemoViewFolder()
    {
        $demoViewPath = Config::get('generator.path_views', base_path('resources/views/')) . "/demo";

        $demoViewFolder = "demo";

        $this->publishDirectory($demoViewFolder, $demoViewPath, 'Demo View Folder');
    }


    // INI - RSM - 20160124 - Publicar los ficheros demo de las vistas frontend
    /**
     * Publishes demo files views
     */
    private function publishFrontEndDemoFilesViews()
    {
        $templateHelper = new TemplatesHelper();
        $fileHelper = new FileHelper();

        $partials = [
            "inicio.blade",
            "quienes-somos.blade",
            "equipo.blade",
            "noticias.blade",
            "contacto.blade"
        ];

        $filePath = Config::get('generator.path_views', base_path('resources/views'))."/demo/";

        foreach($partials as $partial) {
            $templateData = $templateHelper->getTemplate($partial, 'frontend/views/demo');
            $templateData = GeneratorUtils::fillTemplate(CommandData::getConfigDynamicVariables(), $templateData);
            $fileName = $partial.'.php';

            $fileHelper->writeFile($filePath.$fileName, $templateData);
            $this->comment("$partial generated");

        }

    }
    // INI - RSM - 20160124 - Publicar los ficheros demo de las vistas frontend

    // INI - RSM - 20160126 - Publicar el controlador demo del FrontEnd
    private function publishFrontDemoController()
    {
        $templateHelper = new TemplatesHelper();
        $templateData = $templateHelper->getTemplate('FrontDemoController', 'controller');

        $templateData = GeneratorUtils::fillTemplate(CommandData::getConfigDynamicVariables(), $templateData);

        $fileName = 'FrontDemoController.php';

        $filePath = Config::get('generator.path_controller', app_path('Http/Controllers'));

        $fileHelper = new FileHelper();
        $fileHelper->writeFile($filePath.$fileName, $templateData);
        $this->comment("\n$fileName generated");

    }
    // INI - RSM - 20160126 - Publicar el controlador demo del FrontEnd

    //</editor-fold>

}