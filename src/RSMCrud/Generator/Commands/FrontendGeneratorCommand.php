<?php

namespace RSMCrud\Generator\Commands;

use RSMCrud\Generator\CommandData;
use RSMCrud\Generator\Generators\Frontend\FrontViewGenerator;
use RSMCrud\Generator\Generators\Frontend\FrontViewControllerGenerator;
use RSMCrud\Generator\Generators\Common\RoutesGenerator;


class FrontendGeneratorCommand extends BaseCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'rsmcrud.generator:frontend';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Frontend routes, controllers and views';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();

        $this->commandData = new CommandData($this, CommandData::$COMMAND_TYPE_FRONTEND);
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        parent::handle();

        $this->info("Frontend Generator");

        $frontViewControllerGenerator = new FrontViewControllerGenerator($this->commandData);
        $frontViewControllerGenerator->generate();

        $viewsGenerator = new FrontViewGenerator($this->commandData);
        $viewsGenerator->generate();

        $routeGenerator = new RoutesGenerator($this->commandData);
        $routeGenerator->generate();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array_merge(parent::getArguments());
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    public function getOptions()
    {
        return array_merge(parent::getOptions(), []);
    }
}
