<?php

namespace RSMCrud\Generator;

use Config;
use Illuminate\Support\Str;
use RSMCrud\Generator\Commands\APIGeneratorCommand;
use RSMCrud\Generator\File\FileHelper;
use RSMCrud\Generator\Utils\GeneratorUtils;

class CommandData
{
    public $modelName;
    public $modelNamePlural;
    public $modelNameCamel;
    public $modelNamePluralCamel;
    public $modelNamespace;

    // INI - RSM - 20160123 - Formatos para generar las tablas en snake_case y url amigables
    public $modelNameSnakeCase;
    public $modelNameSlug;
    public $modelNamePluralSnakeCase;
    public $modelNamePluralSlug;
    // FIN - RSM - 20160123 - Formatos para generar las tablas en snake_case y url amigables

    public $tableName;
    public $fromTable;
    public $skipMigration;
    public $inputFields;

    // INI - RSM - 20160123 - Crear condicional para que no pregunta por la migración si se ejecuta el comando "--skipMigration"
    public $skipMigrationDatabase;
    // FIN - RSM - 20160123 - Crear condicional para que no pregunta por la migración si se ejecuta el comando "--skipMigration"

    /** @var  string */
    public $commandType;

    /** @var  APIGeneratorCommand */
    public $commandObj;

    /** @var FileHelper */
    public $fileHelper;

    /** @var TemplatesHelper */
    public $templatesHelper;

    /** @var  bool */
    public $useSoftDelete;

    /** @var  bool */
    public $paginate;

    /** @var  string */
    public $rememberToken;

    /** @var  string */
    public $fieldsFile;

    /** @var array */
    public $dynamicVars = [];

    public static $COMMAND_TYPE_API = 'api';
    public static $COMMAND_TYPE_SCAFFOLD = 'scaffold';
    public static $COMMAND_TYPE_SCAFFOLD_API = 'scaffold_api';

    // INI - RSM - 20160123 - Comando para crear rutas, controlador y vistas del frontend
    public static $COMMAND_TYPE_FRONTEND = 'frontend';
    // FIN - RSM - 20160123 - Comando para crear rutas, controlador y vistas del frontend

    public function __construct($commandObj, $commandType)
    {
        $this->commandObj = $commandObj;
        $this->commandType = $commandType;
        $this->fileHelper = new FileHelper();
        $this->templatesHelper = new TemplatesHelper();
    }

    public function initVariables()
    {
        $this->modelNamePlural = Str::plural($this->modelName);
        $this->modelNameCamel = Str::camel($this->modelName);
        $this->modelNamePluralCamel = Str::camel($this->modelNamePlural);

        // INI - RSM - 20160123 - Formatos para generar las tablas en snake_case y url amigables
        $this->modelNameSnakeCase = Str::snake($this->modelName);
        $this->modelNameSlug = Str::slug($this->modelNameSnakeCase);

        $this->modelNamePluralSnakeCase = Str::snake($this->modelNamePluralCamel);
        $this->modelNamePluralSlug = Str::slug($this->modelNamePluralSnakeCase);
        // FIN - RSM - 20160123 - Formatos para generar las tablas en snake_case y url amigables

        $this->initDynamicVariables();
    }

    public function getInputFields()
    {
        $fields = [];

        $this->commandObj->info('Specify fields for the model (skip id & timestamp fields, will be added automatically)');
        $this->commandObj->info('Enter exit to finish');

        while (true) {
            $fieldInputStr = $this->commandObj->ask('Field: (field_name:field_database_type)', '');

            if (empty($fieldInputStr) || $fieldInputStr == false || $fieldInputStr == 'exit') {
                break;
            }

            if (!GeneratorUtils::validateFieldInput($fieldInputStr)) {
                $this->commandObj->error('Invalid Input. Try again');
                continue;
            }

            $type = $this->commandObj->ask('Enter field html input type (text): ', 'text');

            $validations = $this->commandObj->ask('Enter validations: ', false);

            $validations = ($validations == false) ? '' : $validations;

            $fields[] = GeneratorUtils::processFieldInput($fieldInputStr, $type, $validations);
        }

        return $fields;
    }

    public function initDynamicVariables()
    {
        $this->dynamicVars = self::getConfigDynamicVariables();

        $this->dynamicVars = array_merge($this->dynamicVars, [
            '$MODEL_NAME$'              => $this->modelName,

            '$MODEL_NAME_CAMEL$'        => $this->modelNameCamel,

            '$MODEL_NAME_PLURAL$'       => $this->modelNamePlural,

            '$MODEL_NAME_PLURAL_CAMEL$' => $this->modelNamePluralCamel,

            // INI - RSM - 20160119 - URL amigables para el fichero de rutas ("minusculas y guion medio")
            //'$MODEL_NAME_PLURAL_URL$' => Str::slug(snake_case($this->modelNamePluralCamel)),
            // INI - RSM - 20160123 - Formatos para generar las tablas en snake_case y url amigables
            '$MODEL_NAME_PLURAL_URL$' => $this->modelNamePluralSlug,

            '$MODEL_NAME_URL$' => $this->modelNameSlug,
            // FIN - RSM - 20160123 - Formatos para generar las tablas en snake_case y url amigables
            // FIN - RSM - 20160119 - URL amigables para el fichero de rutas ("minusculas y guion medio")

            '$MODEL_NAME_SNAKE_CASE$' => $this->modelNameSnakeCase,
        ]);

        if ($this->tableName) {
            $this->dynamicVars['$TABLE_NAME$'] = $this->tableName;
        } else {

            // INI - RSM - 20160119 - Formatear el nombre de la tabla con snake_case ("guion bajo")
            //$this->dynamicVars['$TABLE_NAME$'] = $this->modelNamePluralCamel;
            // INI - RSM - 20160123 - Formatos para generar las tablas en snake_case y url amigables
            //$this->dynamicVars['$TABLE_NAME$'] = snake_case($this->modelNamePluralCamel);
            $this->dynamicVars['$TABLE_NAME$'] = $this->modelNamePluralSnakeCase;
            // FIN - RSM - 20160123 - Formatos para generar las tablas en snake_case y url amigables
            // FIN - RSM - 20160119 - Formatear el nombre de la tabla con snake_case ("guion bajo")

        }


    }

    public function addDynamicVariable($name, $val)
    {
        $this->dynamicVars[$name] = $val;
    }

    /**
     * RSM
     * @version 1.1 Se han incluido las siguientes variables:
     * '$NAMESPACE_ADMIN_CONTROLLER$'
     * '$ADMIN_PREFIX$'
     *
     * @return array
     */
    public static function getConfigDynamicVariables()
    {
        return [

            '$BASE_CONTROLLER$'                 => Config::get('generator.base_controller', 'RSMCrud\Controller\AppBaseController'),

            '$NAMESPACE_CONTROLLER$'            => Config::get('generator.namespace_controller', 'App\Http\Controllers'),

            '$NAMESPACE_API_CONTROLLER$'        => Config::get('generator.namespace_api_controller', 'App\Http\Controllers\API'),

            '$NAMESPACE_REQUEST$'               => Config::get('generator.namespace_request', 'App\Http\Requests'),

            '$NAMESPACE_REPOSITORY$'            => Config::get('generator.namespace_repository', 'App\Libraries\Repositories'),

            '$NAMESPACE_MODEL$'                 => Config::get('generator.namespace_model', 'App\Models'),

            '$NAMESPACE_MODEL_EXTEND$'          => Config::get('generator.model_extend_class', 'Illuminate\Database\Eloquent\Model'),

            '$SOFT_DELETE_DATES$'               => "\n\tprotected \$dates = ['deleted_at'];\n",

            '$SOFT_DELETE$'                     => "use SoftDeletes;\n",

            '$SOFT_DELETE_IMPORT$'              => "use Illuminate\\Database\\Eloquent\\SoftDeletes;\n",

            '$API_PREFIX$'                      => Config::get('generator.api_prefix', 'api'),

            '$API_VERSION$'                     => Config::get('generator.api_version', 'v1'),

            '$PRIMARY_KEY$'                     => 'id',


            // INI - RSM - 20160119 - Personalizacion
            '$NAMESPACE_ADMIN_CONTROLLER$'      => Config::get('generator.namespace_admin_controller', 'App\Http\Controllers\Admin'),

            '$ADMIN_PREFIX$'                    => Config::get('generator.admin_prefix', 'admin'),
            // FIN - RSM - 20160119 - Personalizacion

            // INI - RSM - 20160123 - Creaccion de un controlador para el Frontend
            '$FRONT_BASE_CONTROLLER$'           => Config::get('generator.front_base_controller', 'RSMCrud\Controller\FrontAppBaseController'),
            // FIN - RSM - 20160123 - Creaccion de un controlador para el Frontend

            // INI - RSM - 20160124 - Creaccion de un controlador para el Frontend
            '$FRONT_VIEW_LAYOUT_BASE_FOLDER$'          => Config::get('generator.path_views_layout', 'resources\views\layouts'),

            '$FRONT_VIEW_PARTIALS_FOLDER$'             => Config::get('generator.path_views_layout', 'resources\views\partials'),
            // FIN - RSM - 20160124 - Creaccion de un controlador para el Frontend

        ];
    }
}
