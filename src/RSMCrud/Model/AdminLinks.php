<?php

namespace RSMCrud\Model;

use Illuminate\Database\Eloquent\Model as Model;


class AdminLinks extends Model
{

	public $table = "admin_links";


	public $fillable = [
		"code",
		"group",
		"langs",
		"title",
		"url",
		"icon"
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		"code" => "string",
		"group" => "string",
		"langs" => "integer",
		"title" => "string",
		"url" => "string",
		"icon" => "string"
	];

	public static $rules = [

	];

	public function getMenu($lang){

		$langsTable = 'langs';
		$linksGroup = 'admin_links_group';

		return \DB::table($this->table)
			->join($langsTable, "$langsTable.id", '=', "$this->table.$langsTable"."_id")
			->join($linksGroup, "$linksGroup.id", '=', "$this->table.$linksGroup"."_id")
			->where('iso_639_1',$lang)
			->orderBy("$linksGroup.order", 'ASC')
			->orderBy("$this->table.order", 'ASC')
			->select("$this->table.id", "$this->table.parent_id", 'group_name', 'title', 'url', 'icon')
			->get();

	}

}
