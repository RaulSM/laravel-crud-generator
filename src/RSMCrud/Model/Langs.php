<?php

namespace RSMCrud\Model;

use Illuminate\Database\Eloquent\Model as Model;

class Langs extends Model
{
    
	public $table = "langs";
    

	public $fillable = [
	    "name_english",
		"name_native",
		"language_family",
		"iso_639_1",
		"iso_639_2_t",
		"enable"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "name_english" => "string",
		"name_native" => "string",
		"language_family" => "string",
		"iso_639_1" => "string",
		"iso_639_2_t" => "string",
		"enable" => "boolean"
    ];

	public static $rules = [
	    "name_english" => "required",
		"name_native" => "required",
		"language_family" => "required",
		"iso_639_1" => "required",
		"iso_639_2_t" => "required"
	];

}
