<?php

namespace RSMCrud\Controller;

use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

// INI - RSM - 20160120 - Incluir model para administrar el menu de administracion
use RSMCrud\Model\AdminLinks;
// FIN - RSM - 20160120 - Incluir model para administrar el menu de administracion

// INI - RSM - 20160417 - Save files
use Illuminate\Support\Facades\Input;
// FIN - RSM - 20160417 - Save files

class AppBaseController extends Controller
{

    /**
     * Validate request for current resource.
     *
     * @param Request $request
     * @param array   $rules
     * @param array   $messages
     * @param array   $customAttributes
     */
    public function validateRequestOrFail($request, array $rules, $messages = [], $customAttributes = [])
    {
        $validator = $this->getValidationFactory()->make($request->all(), $rules, $messages, $customAttributes);

        if ($validator->fails()) {
            throw new HttpException(400, json_encode($validator->errors()->getMessages()));
        }
    }

    public function makeResponse($result, $message)
    {
        return [
            'data'    => $result,
            'message' => $message,
        ];
    }

    public function sendResponse($result, $message)
    {
        return Response::json($this->makeResponse($result, $message));
    }


    // INI - RSM - 20160119 - Personalizacion
    public $menuAdmin;

    // INI - RSM - 20160207 - Guardar el usuario para pasarlo a las vistas
    public $userLogged;
    // FIN - RSM - 20160207 - Guardar el usuario para pasarlo a las vistas

    /**
     * @return string
     */
    public function getMenuAdmin() {
        return $this->menuAdmin;
    }

    /**
     * @param string $menuAdmin
     */
    public function setMenuAdmin($menuAdmin) {
        $this->menuAdmin = $menuAdmin;
    }

    public function setUserLogged($userLogged) {
        $this->userLogged = $userLogged;
    }

    public function getUserLogged() {
        return $this->userLogged;
    }

    public function __construct() {

        $adminLinksClass = new AdminLinks();
        $adminLinks = $adminLinksClass->getMenu(\App::getLocale());
        //dd($adminLinks);

        $menuArray = [];
        foreach($adminLinks as $menuItem){

            $menuArray[ $menuItem->group_name ] [$menuItem->parent_id] [$menuItem->id] = [
                'id' => $menuItem->id,
                'parent_id' => $menuItem->parent_id,
                'title' => $menuItem->title,
                'url' => $menuItem->url,
                'icon' => $menuItem->icon,
            ];

        }
        //dd($menuArray);

        $this->setMenuAdmin($menuArray);

        // INI - RSM - 20160207 - Guardar el usuario para pasarlo a las vistas
        $this->setUserLogged(\Auth::user());
        // FIN - RSM - 20160207 - Guardar el usuario para pasarlo a las vistas
    }
    // FIN - RSM - 20160119 - Personalizacion

    // INI - RSM - 20160417 - Save files
    /**
     * Función para guardar los ficheros que se suben por BackOffice
     * @param $files $request->all()
     * @return mixed $request->all()
     */
    public function saveFiles($files) {

        foreach ($files as $key => $value) {

            if (Input::hasFile($key)) {

                $destinationPath = public_path().'/files/';
                $filename = date('YmdHis-') . $value->getClientOriginalName();
                $value->move($destinationPath, $filename);
                $files[$key] = $filename;
            }
        }
        unset($key);
        unset($value);

        return $files;
    }
    // FIN - RSM - 20160417 - Save files


}
