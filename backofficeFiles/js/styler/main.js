$(document).ready(function(){

    // Cargar campos de fecha jquery-ui
    loadDateFields();

    // Cargar campos de fecha y hora jquery-ui
    loadDateTimeFields();

    // Cardar el editor WYSIWYG para los textarea
    loadWYSIWYGFields();

    // re-styling file elements

    $(':file').filestyle({
        icon: 'fa fa-folder-open',
        classButton: 'btn btn-default'
    });

    // Detectar cuando se modifica un file para mostrar la preview
    $('.imagen-input-file').change(function () {
        readURL(this, $(this));
    });

    // Eliminar el archivo
    $('.input-file-detete').click(function(e){
        e.preventDefault();

        bootbox.confirm("Are you sure?", function(result) {
            if(result === true) {
                removeFile($(this));
            }
        });
    });

});

function loadWYSIWYGFields() {

    if ($('textarea.wysiwyg').length) {

        $('textarea.wysiwyg').summernote({
            height: 200,
            onImageUpload: function(files, editor, welEditable) {
                sendFile(files[0], editor, welEditable);
            }
        });
    }
}

function sendFile(file, editor, welEditable) {
    var data = new FormData();
    data.append("file", file);

    $.ajax({
        data: data,
        type: "POST",
        url: "/admin/ajax/summernote-image-upload.php",
        cache: false,
        contentType: false,
        processData: false,
        success: function(url) {
            editor.insertImage(welEditable, url);
        }
    });
}


/**
 * Funcion para leer la ruta del fichero subido y poder actualizar la imagen.
 * @param {type} input
 * @param {type} input2
 * @returns {undefined}
 */
function readURL(input, input2) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();
        var imgThumbContainer = input2.next().next('.img-thumb-container');

        reader.onload = function (e) {
            imgThumbContainer.find('a').attr('href', e.target.result);
            imgThumbContainer.find('img').attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function removeFile(item){

    var $control = item.parent().parent().children('input[type=file]');
    console.log("eliminar");
    $control.next('.bootstrap-filestyle ').children('input').val("");
    $control.next('.bootstrap-filestyle ').next('.img-thumb-container').find('img.img-thumb').attr('src','/admin/img/no-image.png');
    $control.replaceWith( $control = $control.clone( true ) );

    return false;

}
