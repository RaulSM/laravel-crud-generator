<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Base Controller
    |--------------------------------------------------------------------------
    |
    | This controller will be used as a base controller of all controllers
    |
    */

    'base_controller' => 'RSMCrud\Controller\AppBaseController',

    /*
    |--------------------------------------------------------------------------
    | Path for classes
    |--------------------------------------------------------------------------
    |
    | All Classes will be created on these relevant path
    |
    */

    'path_migration' => base_path('database/migrations/'),

    'path_model' => app_path('Models/'),

    'path_repository' => app_path('Libraries/Repositories/'),

    'path_controller' => app_path('Http/Controllers/'),

    'path_api_controller' => app_path('Http/Controllers/API/'),

    'path_views' => base_path('resources/views'),

    'path_request' => app_path('Http/Requests/'),

    'path_routes' => app_path('Http/routes.php'),

    'path_api_routes' => app_path('Http/api_routes.php'),


    // INI - RSM - 20160119 - Personalizacion
    'path_admin_controller' => app_path('Http/Controllers/Admin/'),
    'path_admin_views' => base_path('resources/views/admin'),
    'path_admin_views_layout' => base_path('resources/views/admin/layouts'),
    'path_admin_views_partials' => base_path('resources/views/admin/partials'),
    'path_admin_routes' => app_path('Http/admin_routes.php'),
    'path_admin_public' => base_path('public/admin/'),
    // FIN - RSM - 20160119 - Personalizacion

    // INI - RSM - 20160120 - Incluir path seeds para publicar langs y admin_links
    'path_seeds' => base_path('database/seeds/'),
    // FIN - RSM - 20160120 - Incluir path seeds para publicar langs y admin_links

    // INI - RSM - 20160121 - Incluir la ruta de idiomas
    'path_langs' => base_path('resources/lang/'),
    // FIN - RSM - 20160121 - Incluir la ruta de idiomas

    // INI - RSM - 20160123 - Creaccion de un controlador para el Frontend
    'front_base_controller' => 'RSMCrud\Controller\FrontAppBaseController',
    // FIN - RSM - 20160123 - Creaccion de un controlador para el Frontend

    // INI - RSM - 20160124 - Creaccion de un controlador para el Frontend
    'path_views_layout_folder' => base_path('resources/views/layouts'),
    'path_views_partials_folder' => base_path('resources/views/partials'),
    // FIN - RSM - 20160124 - Creaccion de un controlador para el Frontend

    /*
    |--------------------------------------------------------------------------
    | Namespace for classes
    |--------------------------------------------------------------------------
    |
    | All Classes will be created with these namespaces
    |
    */

    'namespace_model' => 'App\Models',

    'namespace_repository' => 'App\Libraries\Repositories',

    'namespace_controller' => 'App\Http\Controllers',

    'namespace_api_controller' => 'App\Http\Controllers\API',

    'namespace_request' => 'App\Http\Requests',

    // INI - RSM - 20160119 - Personalizacion
    'namespace_admin_controller' => 'App\Http\Controllers\Admin',
    // FIN - RSM - 20160119 - Personalizacion

    /*
    |--------------------------------------------------------------------------
    | Model extend
    |--------------------------------------------------------------------------
    |
    | Model extend Configuration.
    | By default Eloquent model will be used.
    | If you want to extend your own custom model then you can specify "model_extend" => true and "model_extend_namespace" & "model_extend_class".
    |
    | e.g.
    | 'model_extend' => true,
    | 'model_extend_namespace' => 'App\Models\AppBaseModel as AppBaseModel',
    | 'model_extend_class' => 'AppBaseModel',
    |
    */

    'model_extend_class' => 'Illuminate\Database\Eloquent\Model',

    /*
    |--------------------------------------------------------------------------
    | API routes prefix
    |--------------------------------------------------------------------------
    |
    | By default "api" will be prefix
    |
    */

    'api_prefix' => 'api',

    'api_version' => 'v1',

    /*
    |--------------------------------------------------------------------------
    | dingo API integration
    |--------------------------------------------------------------------------
    |
    | By default dingo API Integration will not be enabled. Dingo API is in beta.
    |
    */

    'use_dingo_api' => false,

    // INI - RSM - 20160119 - Personalizacion
    /*
   |--------------------------------------------------------------------------
   | ADMIN Routes prefix
   |--------------------------------------------------------------------------
   |
   | By default "admin" will be prefix
   |
   */

    'admin_prefix' => 'admin',
    // FIN - RSM - 20160119 - Personalizacion
];
